var marssoft = '';
$(document).ready(() => {
  moment.locale('es');
  marssoft = new Marssoft();
  marssoft.logged();
});

class Marssoft {
  constructor() {
    this.user = '';
    this.data = {
      title: 'Software base',
      url: 'https://client.armesis.gov.mx/base/'
    };
    this.ui = new ui();
    this.asyn = new Ax('services/');
    this.fecha = new Fecha();
    this.out = {
      login: new Login()
    };
    this.in = {
      principal : new Mainframe()
    }
    this.states = [
      ["AS", "AGUASCALIENTES"],
      ["BC", "BAJA CALIFORNIA"],
      ["BS", "BAJA CALIFORNIA SUR"],
      ["CC", "CAMPECHE"],
      ["CH", "CHIHUAHUA"],
      ["CL", "COAHUILA"],
      ["CM", "COLIMA"],
      ["CS", "CHIAPAS"],
      ["DF", "CIUDAD DE MÉXICO O DF"],
      ["DG", "DURANGO"],
      ["GR", "GUERRERO"],
      ["GT", "GUANAJUATO"],
      ["HG", "HIDALGO"],
      ["JC", "JALISCO"],
      ["MC", "MÉXICO"],
      ["MN", "MICHOACÁN"],
      ["MS", "MORELOS"],
      ["NL", "NUEVO LEÓN"],
      ["NT", "NAYARIT"],
      ["OC", "OAXACA"],
      ["PL", "PUEBLA"],
      ["QO", "QUERÉTARO"],
      ["QR", "QUINTANA ROO"],
      ["SL", "SINALOA"],
      ["SP", "SAN LUÍS POTOSÍ"],
      ["SR", "SONORA"],
      ["TC", "TABASCO"],
      ["TL", "TLAXCALA"],
      ["TS", "TAMAULIPAS"],
      ["VZ", "VERACRUZ"],
      ["YN", "YUCATÁN"],
      ["ZS", "ZACATECAS"]
    ];
    this.sidenavLoaded = false;
    this.clickEvents();
  }

  static l(allow = []){
    let p = parseInt($.cookie('use_profile'));
    return allow.indexOf(p);
  }

  static reorder_aggrid(){
    if ($(window).width() > 950){
      let body = $(window).height() - $('.header').height() - $('.cicle_actual').height() - $('.copyright-item').height() - 200;
      $('#myGrid:not(.not_resize)').css('height', (body) + 'px');
      $('.hiddable').addClass('hidden');
    } else {
      $('.hiddable').removeClass('hidden');
    }
  }

  logged() {
    marssoft.asyn.post({ class: 'login', method: 'logged' }, (r) => {
      marssoft.user = r.user_data;
      z.observer('span.privilegies', () => {
        $('span.privilegies').html(marssoft.user.use_id+' - '+marssoft.user.use_profile+' - Último acceso: '+marssoft.user.use_last_login);
      });
      if (!r.response)
        this.out.login.run();
      else
        this.in.principal.run();
    });
  }

  clickEvents() {
    u.keyup('input, textarea', this.removeValidated);
    u.blur('input, textarea', this.setValidations);
    u.keyup('.username, .password', this.checkEnter);
    u.click('.printArea', this.printArea);
    $(window).resize(Marssoft.reorder_aggrid)
  }

  printArea(v){
    let id = v.attr('data-print');
    $('.'+id).printArea({
      extraCss: 'assets/css/materialize.css,assets/css/index.css',
      mode: 'popup',
      popClose: true,
      popHt: 800,
      popWd: 1000
    });
  }

  checkEnter(v, e){
    if (e.keyCode == 13)
      $('.login_action').click();
  }

  removeValidated(v) {
    /** Start of length validation */
    let length = v.attr('data-length');
    if (length != undefined){
      length = length.toSimpleNumber();
      if (v.val().length > length){
        let text = v.val().substring(0, length);
        v.val(text);
      }
    }
    /** end of length validation */
    v.removeClass('required');
  }
  setValidations(v, e){
    switch(e.keyCode){
      case 37:
      case 38:
      case 39:
      case 40:
      case 8:

      break;
      default:
        marssoft.removeValidated(v);
        let val = v.val();
        if (v.hasClass('currency_format')){
          val = val.toSimpleNumber();
          if (isNaN(val))
            val = '$0.00';
          v.val(val.formatMoney());
        }
      break;
    }
  }
}

class Mainframe{
  constructor() {
    this.clickEvents();
    this.activeClass = () => {};
    this.subs = {
      user : new User(),
      client : new Client(),
      supplier : new Supplier(),
      family : new Family(),
      subfamily : new Subfamily(),
    };
    this.DOMAction = '';
    this.DOMAction2 = '';
    this.DOMAction3 = '';
    this.currentPrincipal = '';
    this.tag = '';
  }
  clickEvents() {
    u.click('.action_menu', this.action_menu);
    u.click('.refresh_module', this.refresh_module);
    u.click('.project_selectable', this.project_selectable);
    u.click('.open_popup', this.open_popup);
    u.click('html', this.targeret);
    u.click('.choose', this.choose);
    u.click('.collapse_data', this.collapse_data);
  }
  choose(v){
    let cls = v.attr('class').split(' ');
    cls = cls[cls.length - 1]
    switch(cls){
      case 'settings':
        marssoft.in.principal.settings();
      break;
      case 'close_session':
        marssoft.in.principal.exit_session();
      break;
    }
    $('.window-popup').fadeOut(300);
  }

  settings(){
    marssoft.ui.question('Cambiar contraseña', `
      <div class="row">
        <div class="col s12 input-field">
          ${Form.input('current_password', 'password', 'Contraseña actual', '', '', '', 30, '', 'autocomplete="new-password"')}
        </div>
        <div class="col s12 input-field">
          ${Form.input('new_password', 'password', 'Contraseña nueva', '', '', '', 30, '', 'autocomplete="new-password"')}
        </div>
        <div class="col s12 input-field">
          ${Form.input('r_new_password', 'password', 'Repetir contraseña nueva', '', '', '', 30, '', 'autocomplete="new-password"')}
        </div>
      </div>
    `,'Cambiar contraseña', () => {
      let d = ['current_password', 'new_password', 'r_new_password'];
      let data = z.gValues(d);
      if (data.new_password != data.r_new_password){
        $('.new_password').val('');
        $('.r_new_password').val('');
        Core.toast('Las nuevas contraseñas no coinciden', 'red');
        return false;
      }
      if (!z.mandatory(data, d, true)){
        Core.toast("Los datos en rojo son obligatorios", "red");
        return false;
      }
      marssoft.asyn.post({ class: 'users', method: 'change_password', data: data}, (r) => {
        switch(r.response){
          case 'true':
            Core.toast('Contraseña cambiada correctamente', 'green');
          break;
          case 'denied':
            Core.toast('Contraseña actual incorrecta. Intente nuevamente', 'red');
          break;
          case 'locked':
            Core.toast("Cuenta bloqueada por exceso de fallos, podrá iniciar sesión "+moment(r.time_at).fromNow(), "red");
            $('.choose.close_session').click();
          break;
        }
      });
    }, () => {});
  }

  targeret(v, e){
    if (!$(e.target).hasClass('not_hide')){
      $('.window-popup').fadeOut(300);
      Core.closeContext();
    }
  }
  open_popup(v){
    let target = v.attr('data-target');
    switch(target){
      case 'options':
        marssoft.in.principal.options();
      break;
    }
  }

  options(){
    $('.window-popup').html(`
      <div class="not_hide col s12">
        <h6 class="not_hide">Opciones de cuenta</h6>
      </div>
      <div class="not_hide col s6 m4 choose settings">
        <i class="not_hide material-icons small">settings</i><br>
        Herramientas
      </div>
      <div class="not_hide col s6 m4 choose action_menu" data-target="audits">
        <i class="not_hide material-icons small">assignment</i><br>
        Registro
      </div>
      <div class="not_hide col s6 m4 choose close_session">
        <i class="not_hide material-icons small">lock</i><br>
        Salir
      </div>
    `);
    $('.window-popup').fadeIn(300);
  }

  exit_session(){
    marssoft.asyn.post({ class: 'login', method: 'logout'}, (r) => {
      switch(r.response){
        case 'true':
          Core.toast("Cerrando sesion...", "green");
          setTimeout(() => {
            var cookies = document.cookie.split(";");
            for (var i = 0; i < cookies.length; i++) {
              var cookie = cookies[i];
              var eqPos = cookie.indexOf("=");
              var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
              document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
            }
            location.reload();
          }, 1000);
        break;
        default:
          Core.toast("Error inesperado, intente más tarde.", "red");
        break;
      }
    });
  }

  animate(fn){
    $('.content').addClass('scale-transition').removeClass('scale-in');
    setTimeout(() => {
      $('.content').addClass('scale-in');
      setTimeout(() => {
        $('.content').removeClass('scale-transition');
      }, 201);
      fn();
    }, 201);
  }

  refresh_module(){
    marssoft.in.principal.animate(() => {
      marssoft.in.principal.activeClass();
    });
  }

  buildBreadcrum(options = []){
    if (marssoft.in.principal.currentPrincipal != '')
      options.unshift(marssoft.in.principal.currentPrincipal)
    let m = $('.titlesHeader');
    let type = "breadcrumb white-text";
    if (options.length == 0){
      m.html(`<a href="#!" class="${type}">Dashboard</a>`);
    } else {
      m.html(``);
      let i = 0;
      let suma = 100;
      $.each(options, (i, e) => {
        suma += (e[1].length * 10);
        if (i > 0)
          suma += 50;
        let cls = z.clsid();
        m.append(`<a href="#!" class="${cls} ${type}">${e[1]}</a>`);
        u.click('.'+cls, () =>{
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.activeClass = e[0];
            e[0]();
          });
        });
        i++;
      });
      m.css('width', suma);
      m.parent().animate({scrollLeft: suma}, 800);
    }
  }

  action_menu(v, e){
    let target = v.attr('data-target');
    if (target == 'null'){
      let guid = v.attr('data-assoc');
      let visible = $('.submenu[data-assoc="'+guid+'"]').is(':visible');
      $('.submenu').slideUp(300);
      if (!visible)
        $('.submenu[data-assoc="'+guid+'"]').slideDown(300);
      return false;
    } else {
      if (marssoft.sidenavLoaded)
        if ($(window).width() < 850)
          $('.sidenav').sidenav('close');
    }
    $('.action_menu').removeClass('active');
    v.addClass('active');
    let openboard = () => {};
    switch(target){
      case 'dashboard':
        openboard = marssoft.in.principal.get_dashboard;
      break;
      case 'users':
        openboard = marssoft.in.principal.subs.user.run;
      break;
      case 'audits':
        openboard = marssoft.in.principal.subs.user.audits;
      break;
      case 'clients':
        openboard = marssoft.in.principal.subs.client.run;
      break;
      case 'suppliers':
        openboard = marssoft.in.principal.subs.supplier.run;
      break;
      case 'families':
        openboard = marssoft.in.principal.subs.family.run;
      break;
      case 'subfamilies':
        openboard = marssoft.in.principal.subs.subfamily.run;
      break;
    }
    marssoft.in.principal.activeClass = openboard;
    marssoft.in.principal.animate(() => {
      openboard();
    });
  }
  get_dashboard(){
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.get_dashboard, 'Dashboard'],
    ]);
    $('.content').html(`
      <div class="col s12">
        <div class="card-panel z-depth-4">
          <div class="row accessBottom">
            <div class="col s12">
              <h6>Bienvenido al dashboard</h6>
            </div>
            <div class="col s12 divider"></div>
          </div>
        </div>
      </div>
    `);
  }
  getMenu(option){
    let menu = '';
    switch(option){
      case '1':
      case '2':
      case '3':
      case '4':
        menu = [
          ['dashboard', 'Dashboard', 'dashboard'],
          ['clients', 'Clientes', 'group'],
          ['suppliers', 'Proveedores', 'assignment_ind'],
          // ['suppliers', 'Proveedores', 'assignment_ind', [
          //   ['subfamilies', 'Sub familias 1', 'collections'],
          //   ['subfamilies', 'Sub familias 2', 'collections'],
          //   ['subfamilies', 'Sub familias 3', 'collections'],
          // ]],
          ['families', 'Familias', 'collections'],
          ['subfamilies', 'Sub familias', 'collections'],
          ['users', 'Usuarios', 'person'],
        ];
      break;
      case '5':
        menu = [
          ['sdashboard', 'Dashboard', 'dashboard interno'],
        ];
      break;
    }
    return menu;
  }

  collapse_data(v){
    if (v.attr('data-target') == 'true'){
      $('.hiddable').addClass('hidden');
      $('.spbottom').addClass('minify_1');
      $('.workspace').addClass('minify_2');
      v.attr('data-target', 'false');
    } else {
      $('.hiddable').removeClass('hidden');
      v.attr('data-target', 'true');
      $('.spbottom').removeClass('minify_1');
      $('.workspace').removeClass('minify_2');
    }
  }

  buildSidebar(option = $.cookie('use_profile')){
    let actions = this.getMenu(option);
    let cont = `
      <li class="scale-transition scale-out" style="height:20px;">
        <a href="#!">
          &nbsp;
        </a>
      </li>
    `;
    $.each(actions, (i, e) => {
      if (e[3] == undefined){
        cont += `
          <li class="action_menu scale-transition scale-out tooltipped" data-position="right" data-tooltip="${e[1]}" data-target="${e[0]}">
            <a href="#!">
              <i class="material-icons primary_color">${e[2]}</i>
              <span class="hiddable hidden">${e[1]}</span>
            </a>
          </li>
        `;
      } else {
        let guid = M.guid();
        let base = '';
        $.each(e[3], (j, k) => {
          base += `
            <li class="action_menu scale-transition scale-out tooltipped" data-position="right" data-tooltip="${k[1]}" data-target="${k[0]}">
              <a href="#!">
                <i class="material-icons primary_color">${k[2]}</i>
                <span class="hiddable hidden">${k[1]}</span>
              </a>
            </li>
          `;
        });
        cont += `
          <li class="action_menu scale-transition scale-out tooltipped" data-position="right" data-tooltip="${e[1]}" data-target="null" data-assoc="${guid}">
            <a href="#!">
              <i class="material-icons primary_color">${e[2]}</i>
              <span class="hiddable hidden">
                ${e[1]}
              </span>
            </a>
          </li>
          <li>
            <ul class="submenu" data-assoc="${guid}">
              ${base}
            </ul>
          </li>
        `;
      }
    });
    return cont;
  }
  run(after = () => {}){
    /*
      1. admin
      2. Directivo
      3. Supervisor general
      4. Supervisor región
      5. Administrador local
     */
    setInterval(() => {
      marssoft.asyn.post({ class: 'alive'}, (r) => {}, () => {}, true);
    }, 60000);
    let menuspecial = '';
    let cont = this.buildSidebar();
    let actions = this.getMenu();
    switch($.cookie('use_profile')){
      case '1':
        menuspecial = `
          <li><a class="primary_color" href="#!">Eventos</a></li>
        `;
      break;
    }
    $('title').html(marssoft.data.title);
    $('.app').html(`
      <div class="header z-depth-1 primary_background white-text">
        <div class="container rsmenu">
          <div class="left-container lc1">
            <div class="icon-base hide-on-med-and-up">
              <a href="#" data-target="slide-out" class="sidenav-trigger">
                <i class="material-icons rsmenu white-text">menu</i>
              </a>
            </div>
            <div class="img-base collapse_data" data-target="false">
              <img src="assets/img/smartplanning_logo_w.png" height="30" />
            </div>
          </div>
          <div class="left-container lc2">
            <div class="titlesHeader white-text"></div>
          </div>
          <div class="right-container not_hide">
            <div class="icon-base open_popup not_hide" data-target="options">
              <i class="material-icons rsmenu not_hide">apps</i>
            </div>
          </div>
          <div class="not_hide window-popup row z-depth-3 grey-text darken-2-text center-align">
            
          </div>
        </div>
      </div>
      <div class="workforce">
        <ul id="slide-out" class="spbottom minify_1 sidenav sidenav-fixed z-depth-0 transparent">
          <div class="changable">
            ${cont}
          </div>
        </ul>
        <div class="workspace minify_2">
          <div class="content scale-transition scale-out row"></div>
          <div class="cicle_actual center-align"><span class="privilegies"></span></div>
          <div class="copyright-item center-align nfixed">Copyright &copy; ${(new Date()).getFullYear()} <a href="https://www.grupomarssoft.com/?ref=sitio_web">Grupo Marssoft</a></div>
        </div>
      </div>
    `);
    $(".dropdown-trigger").dropdown({
      constrainWidth: false
    });
    $('.sidenav').sidenav();
    if ($(window).width() < 950){
      $('.hiddable').removeClass('hidden');
    }
    $('.action_menu').addClass('scale-in');
    $('.tooltipped').tooltip();
    marssoft.sidenavLoaded = true;
    $('.action_menu[data-target="dashboard"]').click();
    setTimeout(() => {
      $('.content').addClass('scale-in');
    }, 150);
    $('.action_menu').addClass('scale-in');
    after();
  }
  dashboard(){
    $('.content').html(`
      <div class="col s12">
        <div class="card-panel z-depth-4">
          <div class="row accessBottom">
            Dashboard aquí
          </div>
        </div>
      </div>
    `);
  }
}

class Util{
  constructor(){

  }
  selectTable(selector, active, after = (selector) => {}){
    let target = selector.parent().attr('class').split(' ');
    target = target[target.length - 1];
    if (!selector[0].hasAttribute("data-target"))
      return false;
    $('.'+target+' > tr').removeClass('active');
    selector.addClass('active');
    let attr = selector.attr('data-target');
    $(active).attr('data-target', attr).removeClass('disabled');
    after(selector);
  }

  static rightAlign(array){
    $.each(array, (i, e) => {
      $('td[data-attr="'+e+'"]').addClass('right-align');
      $('th[data-id="'+e+'"]').addClass('right-align');
    });
  }
  static buildselect(array, a, b){
    let query = [];
    $.each(array, (i,e) => {
      let s_a = Table.keyValue(e, a.split('.'), 0);
      let s_b = '';
      if (Array.isArray(b)){
        $.each(b, (j, k) => {
          s_b += Table.keyValue(e, k.split('.'), 0)+' ';
        });
      } else{
        s_b = Table.keyValue(e, b.split('.'), 0);
      }
      query.push([s_a, s_b]);
    });
    return query;
  }

  static getFile(query){
    if ($(query)[0].files.length > 0){
      if ($(query+'-images > .card > .iterbium').length == 1){
        return {
          document : $(query+'-images > .card > .iterbium').attr('data'),
          doc_type : $(query)[0].files[0].name.split('.').pop()
        }
      } else {
        let items = [];
        $(query+'-images > .card > .iterbium').each(function(){
          let data = $(this).attr('data');
          let title = $(this).children('.card-title').html().split('.');
          items.push({
            document: data,
            doc_type : title[title.length - 1]
          });
        });
        return items;
      }
    } else if ($(query+'-images > .card > .iterbium').length > 0){
      let source = $(query+'-images > .card > .iterbium').attr('src').split('.');
      return {
        document : $(query+'-images > .card > .iterbium').attr('data'),
        doc_type : source[source.length - 1]
      }
    } else {
      return {};
    }
  }
  static fillImage(target, url){
    let id = M.guid();
    $(target+'-images').html(`
      <div class="card">
        <img id="s-${id}" class="iterbium" data="" src="${url}" width="100%" />
      </div>
    `);
    Util.getBase64Image(url, (base64) => {
      $('#s-'+id).attr('data', base64);
    });
  }

  static getBase64Image(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
        var reader = new FileReader();
        reader.onloadend = function() {
            callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }

  static getUtilInfo(same){
    return {
      'className' : same.constructor.name.toLowerCase()
    };
  }

  search_engine(v, same){
    sessionStorage.setItem('table_search_'+Util.getUtilInfo(same).className+'_search', v.val());
    same.rungrid.api.setQuickFilter(v.val());
  }
}

class Family extends Util{
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents(){
    u.click('.new_family', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_family();
    });
    u.click('.edit_family', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_family();
    });
    u.click('.family_create', (v) => {
      this.family_create(true);
    });
    u.click('.family_edit', (v) => {
      this.family_create(false);
    });
    u.click('.delete_family', this.delete_family);
    u.keyup('.search_family', (v) => {
      this.search_engine(v, this);
    });
  }

  delete_family(v){
    let selected = marssoft.in.principal.subs.family.rungrid.api.getSelectedRows();
    let id = v.attr('data-target');
    marssoft.ui.question('Confirmar acción','Está seguro de eliminar a la familia <b>'+selected[0].fam_name+'</b>? Esta eliminación será permanente e irreversible','Eliminar', () => {
			marssoft.asyn.post({ class: 'families', method: 'delete', id: id}, (r) => {
        switch(r.response){
          case 'true':
            Core.toast("familia eliminada con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
          break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
          break;
        }
      });
		}, () => {});
  }

  family_create(v){
    let vars = ['fam_name'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, vars, true)){
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'families', method: ((v) ? 'create' : 'update'), data: data }, (r) => {
      switch(r.response){
        case 'true':
          Core.toast("familia creada con éxito", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.DOMAction = '';
            marssoft.in.principal.activeClass = marssoft.in.principal.subs.family.run;
            marssoft.in.principal.activeClass();
          });
        break;
        default:
          Core.toast("Error inesperado, intente más tarde.", "red");
        break;
      }
    });
  }

  management_family(){
    let is_new = (marssoft.in.principal.DOMAction.hasClass('new_family'));
    let action = ['', '', ''];
    let family = '';
    if (is_new){
      action[0] = 'Crear familia';
      action[1] = 'family_create';
      action[2] = 'Nueva familia';
    } else {
      family = marssoft.in.principal.DOMAction.attr('data-target');
      action[0] = 'Editar familia';
      action[1] = 'family_edit';
      action[2] = 'Editar familia';
    }
    marssoft.in.principal.activeClass = marssoft.in.principal.subs.family.management_family;
    marssoft.in.principal.animate(() => {
      marssoft.in.principal.buildBreadcrum([
        [marssoft.in.principal.subs.family.run, 'Familias'],
        [marssoft.in.principal.subs.family.management_family, action[2]],
      ]);
      $('.content').html(`
        <div class="col s12">
          <div class="card-panel z-depth-4">
            <div class="row">
              <div class="col s12">
                <h6>Datos de la familia</h6>
              </div>
              <div class="col s12 input-field">
                ${Form.input('fam_name', 'text', 'Nombre de la familia', '', '', '', 100)}
              </div>
            </div>
          </div>
        </div>
        <div class="col s12 offset-m9 m3 input-field btn_action">
          ${Form.btn(false, 'waves_effect white_text primary_background', action[1]+' btn100 ', action[0], 'save')}
        </div>
      `);
      if (!is_new){
        marssoft.asyn.post({ class: 'families', method: 'single', id: family}, (r) => {
          $('.fam_name').val(r.fam_name);
          M.updateTextFields();
        });
      }
    });
  }
  
  run(){
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.family.run, 'Familias']
    ]);
    $('.content').html(`
      <div class="col s12">
        <div class="card-panel z-depth-4">
          <div class="row accessBottom">
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text primary_background', 'new_family btn100 ', 'Nuevo', 'add')}
            </div>
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_family btn100 disabled', 'Editar', 'edit')}
            </div>
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text red', 'delete_family btn100 disabled', 'Borrar', 'remove')}
            </div>
            <div class="col s12 m6 input-field search_box">
              ${Form.input('search_family', 'text', 'Buscar...', '', '', '', 100, '', 'autocomplete="off"')}
            </div>
            <div class="col s12 divider"></div>
            <div class="col s12">
              <div id="myGrid" style="height: 600px; width:100%;" class="table_place ag-theme-material"></div>
            </div>
          </div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'families', method: 'get'}, (r) => {
      let columnDefs = [
        {headerName: "Id", field: "fam_id", sort: 'asc'},
        {headerName: "Nombre de la familia", field: "fam_name"},
        {headerName: "Subfamilias asociadas", field: "subfamilies_total"},
      ];
      var gridOptions = Grid.buildGripOptions(columnDefs, r, Family.onSelectionChanged, true, false, false, true, -1, 'single', 'search_family');
      marssoft.in.principal.subs.family.rungrid = gridOptions;
      new agGrid.Grid($('.table_place')[0], gridOptions);
    });
  }

  static onSelectionChanged(){
    var selectedRows = marssoft.in.principal.subs.family.rungrid.api.getSelectedRows();
    Grid.activeButtonsGrid(selectedRows[0], 'fam_id', '.delete_family, .edit_family, .access_family');
  }
}

class Subfamily extends Util{
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents(){
    u.click('.new_subfamily', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_subfamily();
    });
    u.click('.edit_subfamily', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_subfamily();
    });
    u.click('.subfamily_create', (v) => {
      this.subfamily_create(true);
    });
    u.click('.subfamily_edit', (v) => {
      this.subfamily_create(false);
    });
    u.click('.delete_subfamily', this.delete_subfamily);
    u.keyup('.search_subfamily', (v) => {
      this.search_engine(v, this);
    });
  }

  delete_subfamily(v){
    let selected = marssoft.in.principal.subs.subfamily.rungrid.api.getSelectedRows();
    let id = v.attr('data-target');
    marssoft.ui.question('Confirmar acción','Está seguro de eliminar a la subfamilia <b>'+selected[0].sub_name+'</b>? Esta eliminación será permanente e irreversible','Eliminar', () => {
			marssoft.asyn.post({ class: 'subfamilies', method: 'delete', id: id}, (r) => {
        switch(r.response){
          case 'true':
            Core.toast("subfamilia eliminada con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
          break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
          break;
        }
      });
		}, () => {});
  }

  subfamily_create(v){
    let vars = ['sub_name', 'sub_fam_id'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, vars, true)){
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'subfamilies', method: ((v) ? 'create' : 'update'), data: data }, (r) => {
      switch(r.response){
        case 'true':
          Core.toast("subfamilia creada con éxito", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.DOMAction = '';
            marssoft.in.principal.activeClass = marssoft.in.principal.subs.subfamily.run;
            marssoft.in.principal.activeClass();
          });
        break;
        case 'family_not_found':
          Core.toast("La familia padre no existe, intente de nuevo por favor.", "red");
        break;
        case 'Unexpected_error':
          Core.toast("Error inesperado, intente más tarde.", "red");
        break;
      }
    });
  }

  management_subfamily(){
    let is_new = (marssoft.in.principal.DOMAction.hasClass('new_subfamily'));
    let action = ['', '', ''];
    let subfamily = '';
    if (is_new){
      action[0] = 'Crear subfamilia';
      action[1] = 'subfamily_create';
      action[2] = 'Nueva familia';
    } else {
      subfamily = marssoft.in.principal.DOMAction.attr('data-target');
      action[0] = 'Editar subfamilia';
      action[1] = 'subfamily_edit';
      action[2] = 'Editar subfamilia';
    }
    marssoft.in.principal.activeClass = marssoft.in.principal.subs.subfamily.management_subfamily;
    marssoft.in.principal.animate(() => {
      let families = []
      marssoft.asyn.post({ class: 'families', method: 'get'}, (r) => {
        $.each(r, (i, e) => {
          families.push([e.fam_id, e.fam_name]);
        });
        marssoft.in.principal.buildBreadcrum([
          [marssoft.in.principal.subs.subfamily.run, 'Subfamilias'],
          [marssoft.in.principal.subs.subfamily.management_subfamily, action[2]],
        ]);
        $('.content').html(`
          <div class="col s12">
            <div class="card-panel z-depth-4">
              <div class="row">
                <div class="col s12">
                  <h6>Datos de la subfamilia</h6>
                </div>
                <div class="col s12 m6 input-field">
                  ${Form.select('sub_fam_id', 'Seleccione una familia padre', families, '', '', 'searchable="true"')}
                </div>
                <div class="col s12 m6 input-field">
                  ${Form.input('sub_name', 'text', 'Nombre de la subfamilia', '', '', '', 100)}
                </div>
              </div>
            </div>
          </div>
          <div class="col s12 offset-m9 m3 input-field btn_action">
            ${Form.btn(false, 'waves_effect white_text primary_background', action[1]+' btn100 ', action[0], 'save')}
          </div>
        `);
        if (!is_new){
          marssoft.asyn.post({ class: 'subfamilies', method: 'single', id: subfamily}, (r) => {
            $('.sub_name').val(r.sub_name);
            $('.sub_fam_id').val(r.sub_fam_id);
            $('.sub_fam_id').formSelect();
            M.updateTextFields();
          });
        } else {
          $('.sub_fam_id').formSelect();
        }
      });
    });
  }

  run(){
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.subfamily.run, 'Subfamilias']
    ]);
    $('.content').html(`
      <div class="col s12">
        <div class="card-panel z-depth-4">
          <div class="row accessBottom">
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text primary_background', 'new_subfamily btn100 ', 'Nuevo', 'add')}
            </div>
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_subfamily btn100 disabled', 'Editar', 'edit')}
            </div>
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text red', 'delete_subfamily btn100 disabled', 'Borrar', 'remove')}
            </div>
            <div class="col s12 m6 input-field search_box">
              ${Form.input('search_subfamily', 'text', 'Buscar...', '', '', '', 100, '', 'autocomplete="off"')}
            </div>
            <div class="col s12 divider"></div>
            <div class="col s12">
              <div id="myGrid" style="height: 600px; width:100%;" class="table_place ag-theme-material"></div>
            </div>
          </div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'subfamilies', method: 'get'}, (r) => {
      let columnDefs = [
        {headerName: "Id", field: "sub_id"},
        {headerName: "Familia padre", field: "family.fam_name", rowGroup: true, hide: true, pinned: 'left'},
        {headerName: "Nombre de la subfamilia", field: "sub_name", sort: 'asc'},
      ];
      var gridOptions = Grid.buildGripOptions(columnDefs, r, Subfamily.onSelectionChanged, true, false, false, true, -1, 'single', 'search_subfamily');
      marssoft.in.principal.subs.subfamily.rungrid = gridOptions;
      new agGrid.Grid($('.table_place')[0], gridOptions);
    });
  }

  static onSelectionChanged(){
    var selectedRows = marssoft.in.principal.subs.subfamily.rungrid.api.getSelectedRows();
    Grid.activeButtonsGrid(selectedRows[0], 'sub_id', '.delete_subfamily, .edit_subfamily, .access_subfamily');
  }
}

class Supplier extends Util{
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents(){
    u.click('.new_supplier', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_supplier();
    });
    u.click('.edit_supplier', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_supplier();
    });
    u.click('.supplier_create', (v) => {
      this.supplier_create(true);
    });
    u.click('.supplier_edit', (v) => {
      this.supplier_create(false);
    });
    u.click('.delete_supplier', this.delete_supplier);
    u.keyup('.search_supplier', (v) => {
      this.search_engine(v, this);
    });
  }

  delete_supplier(v){
    let selected = marssoft.in.principal.subs.client.rungrid.api.getSelectedRows();
    let id = v.attr('data-target');
    marssoft.ui.question('Confirmar acción','Está seguro de eliminar al provedor <b>'+selected[0].sup_supplier_name+'</b>? Esta eliminación será permanente e irreversible','Eliminar', () => {
			marssoft.asyn.post({ class: 'suppliers', method: 'delete', id: id}, (r) => {
        switch(r.response){
          case 'true':
            Core.toast("provedor eliminado con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
          break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
          break;
        }
      });
		}, () => {});
  }

  supplier_create(v){
    let vars = ['sup_supplier_name', 'sup_contact_name', 'sup_social_reason', 'sup_address', 'sup_rfc', 'sup_bank_info', 'sup_phone_home_1', 'sup_phone_home_2', 'sup_phone_cell', 'sup_fax', 'sup_email'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, ['sup_supplier_name', 'sup_rfc', 'sup_phone_home_1', 'sup_email', 'sup_contact_name'], true)){
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'suppliers', method: ((v) ? 'create' : 'update'), data: data }, (r) => {
      switch(r.response){
        case 'true':
          Core.toast("Usuario creado con éxito", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.DOMAction = '';
            marssoft.in.principal.activeClass = marssoft.in.principal.subs.supplier.run;
            marssoft.in.principal.activeClass();
          });
        break;
        case 'Unexpected_error':
          Core.toast("Error inesperado, intente más tarde.", "red");
        break;
      }
    });
  }

  management_supplier(){
    let is_new = (marssoft.in.principal.DOMAction.hasClass('new_supplier'));
    let action = ['', '', ''];
    let supplier = '';
    if (is_new){
      action[0] = 'Crear proveedor';
      action[1] = 'supplier_create';
      action[2] = 'Nuevo proveedor';
    } else {
      supplier = marssoft.in.principal.DOMAction.attr('data-target');
      action[0] = 'Editar proveedor';
      action[1] = 'supplier_edit';
      action[2] = 'Editar proveedor';
    }
    marssoft.in.principal.activeClass = marssoft.in.principal.subs.supplier.management_supplier;
    marssoft.in.principal.animate(() => {
      marssoft.in.principal.buildBreadcrum([
        [marssoft.in.principal.subs.supplier.run, 'Proveedores'],
        [marssoft.in.principal.subs.supplier.management_supplier, action[2]],
      ]);
      $('.content').html(`
        <div class="col s12">
          <div class="card-panel z-depth-4">
            <div class="row">
              <div class="col s12">
                <h6>Datos del proveedor</h6>
              </div>
              <div class="col s12 input-field">
                ${Form.input('sup_supplier_name', 'text', 'Nombre del proveedor', '', '', '', 200)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m4 input-field">
                ${Form.input('sup_contact_name', 'text', 'Nombre del contacto', '', '', '', 200)}
              </div>
              <div class="col s12 m8 input-field">
                ${Form.input('sup_social_reason', 'text', 'Razón social', '', '', '', 200)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 input-field">
                ${Form.input('sup_address', 'text', 'Dirección', '', '', '', 200)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m4 input-field">
                ${Form.input('sup_rfc', 'text', 'RFC', '', '', '', 15)}
              </div>
              <div class="col s12 m8 input-field">
                ${Form.input('sup_bank_info', 'text', 'Información bancaria', '', '', '', 200)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m3 input-field">
                ${Form.input('sup_phone_home_1', 'text', 'Teléfono principal', '', '', '', 16)}
              </div>
              <div class="col s12 m3 input-field">
                ${Form.input('sup_phone_home_2', 'text', 'Teléfono alterno', '', '', '', 16)}
              </div>
              <div class="col s12 m3 input-field">
                ${Form.input('sup_phone_cell', 'text', 'Teléfono celular', '', '', '', 16)}
              </div>
              <div class="col s12 m3 input-field">
                ${Form.input('sup_fax', 'text', 'Fax', '', '', '', 16)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 input-field">
                ${Form.input('sup_email', 'text', 'Correo electrónico', '', '', '', 200)}
              </div>
            </div>
          </div>
        </div>
        <div class="col s12 offset-m9 m3 input-field btn_action">
          ${Form.btn(false, 'waves_effect white_text primary_background', action[1]+' btn100 ', action[0], 'save')}
        </div>
      `);
      if (!is_new){
        marssoft.asyn.post({ class: 'suppliers', method: 'single', id: supplier}, (r) => {
          $('.sup_supplier_name').val(r.sup_supplier_name);
          $('.sup_contact_name').val(r.sup_contact_name);
          $('.sup_social_reason').val(r.sup_social_reason);
          $('.sup_address').val(r.sup_address);
          $('.sup_rfc').val(r.sup_rfc);
          $('.sup_bank_info').val(r.sup_bank_info);
          $('.sup_phone_home_1').val(r.sup_phone_home_1);
          $('.sup_phone_home_2').val(r.sup_phone_home_2);
          $('.sup_phone_cell').val(r.sup_phone_cell);
          $('.sup_fax').val(r.sup_fax);
          $('.sup_email').val(r.sup_email);
          $('input').characterCounter();
          M.updateTextFields();
        });
      } else {
        $('input').characterCounter();
      }
    });
  }

  run(){
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.supplier.run, 'Proveedores']
    ]);
    $('.content').html(`
      <div class="col s12">
        <div class="card-panel z-depth-4">
          <div class="row accessBottom">
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text primary_background', 'new_supplier btn100 ', 'Nuevo', 'add')}
            </div>
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_supplier btn100 disabled', 'Editar', 'edit')}
            </div>
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text red', 'delete_supplier btn100 disabled', 'Borrar', 'remove')}
            </div>
            <div class="col s12 m6 input-field search_box">
              ${Form.input('search_supplier', 'text', 'Buscar...', '', '', '', 100, '', 'autocomplete="off"')}
            </div>
            <div class="col s12 divider"></div>
            <div class="col s12">
              <div id="myGrid" style="height: 600px; width:100%;" class="table_place ag-theme-material"></div>
            </div>
          </div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'suppliers', method: 'get'}, (r) => {
      let columnDefs = [
        {headerName: "Nombre", field: "sup_supplier_name", sort: 'asc'},
        {headerName: "Contacto", field: "sup_contact_name"},
        {headerName: "Teléfono", field: "sup_phone_home_1"},
        {headerName: "Correo electrónico", field: "sup_email"},
      ];
      var gridOptions = Grid.buildGripOptions(columnDefs, r, Supplier.onSelectionChanged, true, false, false, true, -1, 'single', 'search_supplier');
      marssoft.in.principal.subs.supplier.rungrid = gridOptions;
      new agGrid.Grid($('.table_place')[0], gridOptions);
    });
  }

  static onSelectionChanged(){
    var selectedRows = marssoft.in.principal.subs.supplier.rungrid.api.getSelectedRows();
    Grid.activeButtonsGrid(selectedRows[0], 'sup_id', '.delete_supplier, .edit_supplier, .access_supplier');
  }
}

class Client extends Util{
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents(){
    u.click('.new_client', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_client();
    });
    u.click('.edit_client', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_client();
    });
    u.click('.client_create', (v) => {
      this.client_create(true);
    });
    u.click('.client_edit', (v) => {
      this.client_create(false);
    });
    u.click('.delete_client', this.delete_client);
    u.keyup('.search_client', (v) => {
      this.search_engine(v, this);
    });
  }

  delete_client(v){
    let selected = marssoft.in.principal.subs.client.rungrid.api.getSelectedRows();
    let id = v.attr('data-target');
    marssoft.ui.question('Confirmar acción','Está seguro de eliminar al cliente <b>'+selected[0].fullname+'</b>? Esta eliminación será permanente e irreversible','Eliminar', () => {
			marssoft.asyn.post({ class: 'clients', method: 'delete', id: id}, (r) => {
        switch(r.response){
          case 'true':
            Core.toast("cliente eliminado con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
          break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
          break;
        }
      });
		}, () => {});
  }

  client_create(v){
    let vars = ['cli_first_name', 'cli_last_name', 'cli_sur_name', 'cli_gender', 'cli_birthday', 'cli_address', 'cli_postal_code', 'cli_colony', 'cli_city', 'cli_state', 'cli_country', 'cli_social_reason', 'cli_rfc', 'cli_phone', 'cli_email'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, ['cli_first_name', 'cli_last_name', 'cli_sur_name', 'cli_social_reason', 'cli_rfc', 'cli_phone', 'cli_email'], true)){
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'clients', method: ((v) ? 'create' : 'update'), data: data }, (r) => {
      switch(r.response){
        case 'true':
          Core.toast("cliente creado con éxito", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.DOMAction = '';
            marssoft.in.principal.activeClass = marssoft.in.principal.subs.client.run;
            marssoft.in.principal.activeClass();
          });
        break;
        case 'Unexpected_error':
          Core.toast("Error inesperado, intente más tarde.", "red");
        break;
      }
    });
  }

  management_client(){
    let is_new = (marssoft.in.principal.DOMAction.hasClass('new_client'));
    let action = ['', '', ''];
    let client = '';
    if (is_new){
      action[0] = 'Crear cliente';
      action[1] = 'client_create';
      action[2] = 'Nuevo cliente';
    } else {
      client = marssoft.in.principal.DOMAction.attr('data-target');
      action[0] = 'Editar cliente';
      action[1] = 'client_edit';
      action[2] = 'Editar cliente';
    }
    marssoft.in.principal.activeClass = marssoft.in.principal.subs.client.management_client;
    marssoft.in.principal.animate(() => {
      marssoft.in.principal.buildBreadcrum([
        [marssoft.in.principal.subs.client.run, 'Clientes'],
        [marssoft.in.principal.subs.client.management_client, action[2]],
      ]);
      $('.content').html(`
        <div class="col s12">
          <div class="card-panel z-depth-4">
            <div class="row">
              <div class="col s12">
                <h6>Datos del cliente</h6>
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_first_name', 'text', 'Nombre(s)', '', '', '', 100)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_last_name', 'text', 'Apellido paterno', '', '', '', 100)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_sur_name', 'text', 'Apellido materno', '', '', '', 100)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m3 input-field">
                ${Form.select('cli_gender', 'Género', [[1, 'Mujer'], [2, 'Hombre']])}
              </div>
              <div class="col s12 m3 input-field">
                ${Form.input('cli_birthday', 'text', 'Fecha de nacimiento', '', '', '', 200)}
              </div>
              <div class="col s12 m6 input-field">
                ${Form.input('cli_address', 'text', 'Dirección', '', '', '', 200)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_postal_code', 'number', 'Código postal', '', '', '', 5)}
              </div>
              <div class="col s12 m8 input-field">
                ${Form.input('cli_colony', 'text', 'Colonia', '', '', '', 200)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_city', 'text', 'Ciudad', '', '', '', 100)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_state', 'text', 'Estado', '', '', '', 100)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_country', 'text', 'País', '', '', '', 50)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m8 input-field">
                ${Form.input('cli_social_reason', 'text', 'Razón social', '', '', '', 200)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_rfc', 'text', 'RFC', '', '', '', 15)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_phone', 'number', 'Teléfono', '', '', '', 10)}
              </div>
              <div class="col s12 m8 input-field">
                ${Form.input('cli_email', 'text', 'Correo electrónico', '', '', '', 200)}
              </div>
            </div>
          </div>
        </div>
        <div class="col s12 offset-m9 m3 input-field btn_action">
          ${Form.btn(false, 'waves_effect white_text primary_background', action[1]+' btn100 ', action[0], 'save')}
        </div>
      `);
      if (!is_new){
        marssoft.asyn.post({ class: 'clients', method: 'single', id: client}, (r) => {
          $('.cli_first_name').val(r.cli_first_name);
          $('.cli_last_name').val(r.cli_last_name);
          $('.cli_sur_name').val(r.cli_sur_name);
          $('.cli_address').val(r.cli_address);
          $('.cli_postal_code').val(r.cli_postal_code);
          $('.cli_colony').val(r.cli_colony);
          $('.cli_city').val(r.cli_city);
          $('.cli_state').val(r.cli_state);
          $('.cli_country').val(r.cli_country);
          $('.cli_social_reason').val(r.cli_social_reason);
          $('.cli_rfc').val(r.cli_rfc);
          $('.cli_email').val(r.cli_email);
          marssoft.fecha.Dtpicker('.cli_birthday', r.cli_birthday.date);
          $('.cli_gender').val(r.cli_gender);
          $('.cli_phone').val(r.cli_phone);
          
          $('.cli_gender').formSelect();
          $('input').characterCounter();
          M.updateTextFields();
        });
      } else {
        $('input').characterCounter();
        marssoft.fecha.Dtpicker('.cli_birthday');
        $('.cli_gender').formSelect();
      }
    });
  }

  run(){
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.client.run, 'Clientes']
    ]);
    $('.content').html(`
      <div class="col s12">
        <div class="card-panel z-depth-4">
          <div class="row accessBottom">
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text primary_background', 'new_client btn100 ', 'Nuevo', 'add')}
            </div>
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_client btn100 disabled', 'Editar', 'edit')}
            </div>
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text red', 'delete_client btn100 disabled', 'Borrar', 'remove')}
            </div>
            <div class="col s12 m6 input-field search_box">
              ${Form.input('search_client', 'text', 'Buscar...', '', '', '', 100, '', 'autocomplete="off"')}
            </div>
            <div class="col s12 divider"></div>
            <div class="col s12">
              <div id="myGrid" style="height: 600px; width:100%;" class="table_place ag-theme-material"></div>
            </div>
          </div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'clients', method: 'get'}, (r) => {
      let columnDefs = [
        {headerName: "Nombre", field: "fullname", sort: 'asc'},
        {headerName: "Razón social", field: "cli_social_reason"},
        {headerName: "RFC", field: "cli_rfc"},
        {headerName: "Correo", field: "cli_email"},
        {headerName: "Teléfono", field: "cli_phone"},
      ];
      var gridOptions = Grid.buildGripOptions(columnDefs, r, Client.onSelectionChanged, true, false, false, true, -1, 'single', 'search_client');
      marssoft.in.principal.subs.client.rungrid = gridOptions;
      new agGrid.Grid($('.table_place')[0], gridOptions);
    });
  }

  static onSelectionChanged(){
    var selectedRows = marssoft.in.principal.subs.client.rungrid.api.getSelectedRows();
    Grid.activeButtonsGrid(selectedRows[0], 'cli_id', '.delete_client, .edit_client, .access_client');
  }
}

class User extends Util{
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents(){
    u.click('.new_user', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_user();
    });
    u.click('.edit_user', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_user();
    });
    u.click('.user_create', (v) => {
      this.user_create(true);
    });
    u.click('.user_edit', (v) => {
      this.user_create(false);
    });
    u.click('.delete_user', this.delete_user);
    u.click('.access_user', this.access_user);
    u.click('.filter_user', this.filter_user);
    u.keyup('.search_user', (v) => {
      this.search_engine(v, this);
    });
    u.change('.use_module', this.use_module);
    u.click('.detail_usage', this.detail_usage);
  }

  access_user(v){
    let id = v.attr('data-target');
    marssoft.asyn.post({ class: 'users', method: 'changeUser', id: id}, (r) => {
      switch(r.response){
        case 'true':
          Core.toast("Bienvenido, acceso correcto...", "green");
          setTimeout(() => {
            location.reload();
          },3000);
        break;
        default:
          Core.toast("Acceso denegado", "red");
        break;
      }
    });
  }

  delete_user(v){
    let selected = marssoft.in.principal.subs.user.rungrid.api.getSelectedRows();
    let id = v.attr('data-target');
    marssoft.ui.question('Confirmar acción','Está seguro de eliminar al usuario <b>'+selected[0].fullname+'</b>? Esta eliminación será permanente e irreversible','Eliminar', () => {
			marssoft.asyn.post({ class: 'users', method: 'delete', id: id}, (r) => {
        switch(r.response){
          case 'true':
            Core.toast("Usuario eliminado con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
          break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
          break;
        }
      });
		}, () => {});
  }

  user_create(v){
    let vars = ['per_firstname', 'per_lastname', 'per_surname', 'per_birthday', 'per_gender', 'per_phone', 'per_email', 'use_id', 'use_profile'];
    if (v || $('.use_password').val() != ''){
      vars.push('use_password');
      vars.push('confirm_password');
    }
    let data = z.gValues(vars);
    if (!z.mandatory(data, vars, true)){
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    if (v || $('.use_password').val() != ''){
      if (data.use_password != data.confirm_password){
        Core.toast("Las constraseñas no coinciden", "red");
        return false;
      }
    }
    data = {
      personals : {
        per_firstname : data.per_firstname,
        per_lastname : data.per_lastname,
        per_surname : data.per_surname,
        per_email : data.per_email,
        per_birthday : data.per_birthday,
        per_gender : data.per_gender,
        per_phone : data.per_phone
      },
      users : {
        use_id : data.use_id,
        use_profile : data.use_profile,
        use_password : data.use_password
      }
    }
    marssoft.asyn.post({ class: 'users', method: ((v) ? 'create' : 'update'), data: data }, (r) => {
      switch(r.response){
        case 'true':
          Core.toast("Usuario creado con éxito", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.DOMAction = '';
            marssoft.in.principal.activeClass = marssoft.in.principal.subs.user.run;
            marssoft.in.principal.activeClass();
          });
        break;
        case 'user_exists':
          Core.toast("El nombre de usuario ya se encuentra ocupado. Intente con otro", "red");
          $('.use_id').val('');
        break;
        case 'Unexpected_error':
          Core.toast("Error inesperado, intente más tarde.", "red");
        break;
      }
    });
  }

  management_user(){
    let is_new = (marssoft.in.principal.DOMAction.hasClass('new_user'));
    let action = ['', '', ''];
    let user = '';
    if (is_new){
      action[0] = 'Crear usuario';
      action[1] = 'user_create';
      action[2] = 'Nuevo usuario';
    } else {
      user = marssoft.in.principal.DOMAction.attr('data-target');
      action[0] = 'Editar usuario';
      action[1] = 'user_edit';
      action[2] = 'Editar usuario';
    }
    marssoft.in.principal.activeClass = marssoft.in.principal.subs.user.management_user;
    marssoft.in.principal.animate(() => {
      marssoft.in.principal.buildBreadcrum([
        [marssoft.in.principal.subs.user.run, 'Usuarios'],
        [marssoft.in.principal.subs.user.management_user, action[2]],
      ]);
      $('.content').html(`
        <div class="col s12">
          <div class="card-panel z-depth-4">
            <div class="row">
              <div class="col s12">
                <h6>Datos personales</h6>
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('per_firstname', 'text', 'Nombre(s)', '', '', '', 100)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('per_lastname', 'text', 'Apellido paterno', '', '', '', 100)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('per_surname', 'text', 'Apellido materno', '', '', '', 100)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m4 input-field">
                ${Form.input('per_birthday', 'text', 'Fecha de nacimiento', '')}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.select('per_gender', 'Género', [[1, 'Mujer'], [2, 'Hombre']])}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('per_phone', 'number', 'Teléfono a 10 dígitos', '', '', '', 10)}
              </div>
              <div class="col s12 input-field">
                ${Form.input('per_email', 'email', 'Correo electrónico', '', '', '', 150)}
              </div>
            </div>
          </div>
        </div>
        <div class="col s12">
          <div class="card-panel z-depth-4">
            <div class="row">
              <div class="col s12">
                <h6>Datos de inicio de sesión</h6>
              </div>
              <div class="col s12 m6 input-field">
                ${Form.input('use_id', 'text', 'Nombre de usuario', '', '', '', 30, '', 'autocomplete="off"')}
              </div>
              <div class="col s12 m3 input-field">
                ${Form.input('use_password', 'password', 'Contraseña', '', '', '', 30, '', 'autocomplete="new-password"')}
              </div>
              <div class="col s12 m3 input-field">
                ${Form.input('confirm_password', 'password', 'Confirmar contraseña', '', '', '', 30, '', 'autocomplete="new-password"')}
              </div>
              <div class="col s12"></div>
              <div class="col s12 input-field">
                ${Form.select('use_profile', 'Nivel de acceso', [[2, 'Dirección'], [3, 'Supervisor de proyecto'], [4, 'Administrativo']])}
              </div>
            </div>
          </div>
        </div>
        <div class="col s12 m3 offset-m9 input-field btn_action">
          ${Form.btn(false, 'waves_effect white_text primary_background', action[1]+' btn100 ', action[0], 'save')}
        </div>
      `);
      if (!is_new){
        marssoft.asyn.post({ class: 'users', method: 'single', id: user}, (r) => {
          $('.use_id').attr('disabled', 'disabled').val(r.use_id);
          $('.per_firstname').val(r.personal.per_firstname);
          $('.per_lastname').val(r.personal.per_lastname);
          $('.per_surname').val(r.personal.per_surname);
          marssoft.fecha.Dtpicker('.per_birthday', r.personal.per_birthday.date);
          $('.per_gender').val(r.personal.per_gender);
          $('.use_profile').val(r.use_profile);
          $('.per_phone').val(r.personal.per_phone);
          $('.per_email').val(r.personal.per_email);
          $('input').characterCounter();
          
          $('.per_gender, .use_profile').formSelect();
          M.updateTextFields();
          console.log(r);
        });
      } else {
        $('input').characterCounter();
        marssoft.fecha.Dtpicker('.per_birthday');
        $('.per_gender, .use_profile').formSelect();
      }
    });
  }

  use_module(v){
    let val = v.val();
    marssoft.asyn.post({ class: 'logs', method: 'getLogsByModule', data: val}, (r) => {
      let modules = Util.buildselect(r, 'id', 'value');
      modules.unshift(['0', 'Todos los detalles']);
      $('.div_user_event').html(Form.select('use_event', 'Seleccione evento', modules, '', '', 'searchable="true"'));
      $('.use_event').val('0');
      $('.use_event').formSelect();
    });
  }

  detail_usage(){
    let row = marssoft.in.principal.subs.user.rungrid.api.getSelectedRows()[0];
    marssoft.ui.question('Detalles del acceso',`
      <table>
        <tr>
          <th>Módulo y acción accedida</th>
          <td>${row.log_class} / ${row.log_special}</td>
        </tr>
        <tr>
          <th>Usuario responsable</th>
          <td>
            ${row.log_use_id} - ${row.fullname} ${row.log_obs}
          </td>
        </tr>
        <tr>
          <th>Fecha del evento</th>
          <td>${row.log_created_at}</td>
        </tr>
        <tr>
          <th>Fecha de inicio de sesión</th>
          <td>
            ${row.log_session_data.current_login || 'No especificado'}
          </td>
        </tr>
        <tr>
          <th>Navegador / S.O. / IP</th>
          <td>${row.log_browser_info.browser} ${row.log_browser_info.version} / ${row.log_browser_info.platform} / ${row.log_ip}</td>
        </tr>
        <tr>
          <th colspan="2" class="center-align">Datos enviados por el usuario</th>
        </tr>
        <tr>
          <td colspan="2"><pre>${JSON.stringify(row.log_post_data, null, 2)}</pre></td>
        </tr>
        <tr>
          <th colspan="2" class="center-align">Información detallada del usuario</th>
        </tr>
        <tr>
          <td colspan="2"><pre>${JSON.stringify(row.log_session_data, null, 2)}</pre></td>
        </tr>
      </table>
    `,'Cerrar', () => {}, () => {});
  }

  audits(){
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.user.audits, 'Registro de eventos']
    ]);
    marssoft.asyn.post({ class: 'logs', method: 'loadResources'}, (r) => {
      let user = Util.buildselect(r.users, 'use_id', 'personal.fullname');
      let modules = Util.buildselect(r.modules, 'id', 'value');
      modules.unshift(['0', 'Todos los módulos']);
      user.unshift(['-1', 'Todos los usuarios']);
      user.unshift(['0', 'Invitados']);
      $('.content').html(`
        <div class="col s12">
          <div class="card-panel z-depth-4">
            <div class="row accessBottom">
              <div class="col s12 m3 input-field">
                ${Form.select('use_filter', 'Seleccione un usuario', user, '', '', 'searchable="true"')}
              </div>
              <div class="col s6 m2 input-field">
                ${Form.input('ser_start_date', 'text', 'Fecha de inicio', '', '', '', 100)}
              </div>
              <div class="col s6 m2 input-field">
                ${Form.input('ser_end_date', 'text', 'Fecha de fin', '', '', '', 100)}
              </div>
              <div class="col s6 m2 input-field">
                ${Form.select('use_module', 'Seleccione un módulo', modules, '', '', 'searchable="true"')}
              </div>

              <div class="col s6 m3 input-field div_user_event">
                ${Form.select('use_event', 'Seleccione evento', [[0, 'Todos los eventos']], '', '', 'searchable="true" disabled="disabled"')}
              </div>
              
              <div class="col s12 m9 input-field search_box">
                ${Form.input('search_user', 'text', 'Buscar...', '', '', '', 100, '', 'autocomplete="off"')}
              </div>
              <div class="col s12 m3">
                ${Form.btn(false, 'waves_effect white_text primary_background', 'filter_user btn100', 'Buscar', 'search')}
              </div>
              <div class="col s6 m2 hidden">
                ${Form.btn(false, 'waves_effect white_text primary_background', 'detail_usage btn100 disabled', 'Detalles', 'dehaze')}
              </div>
              <div class="col s12 divider"></div>
              <div class="col s12">
                <div id="myGrid" style="height: 600px; width:100%;" class="table_place ag-theme-material"></div>
              </div>
            </div>
          </div>
        </div>
      `);
      $('.use_filter').val('-1');
      $('.use_filter').formSelect();
      $('.use_module').val('0');
      $('.use_module').formSelect();
      $('.use_event').val('0');
      $('.use_event').formSelect();
      marssoft.fecha.Dtpicker('.ser_start_date');
      marssoft.fecha.Dtpicker('.ser_end_date');
      marssoft.in.principal.subs.user.getRecords();
    });
  }

  filter_user(){
    marssoft.in.principal.subs.user.getRecords();
  }

  getRecords(){
    let vars = ['use_filter', 'ser_start_date', 'ser_end_date', 'use_module', 'use_event'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, vars, true)){
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    if (!z.compareTwoDates(data.ser_start_date, data.ser_end_date)){
      Core.toast('La fecha de inicio debe ser menor que la fecha de fin', 'red');
      return false;
    }
    marssoft.asyn.post({ class: 'logs', method: 'getLogs', data: data}, (r) => {
      let columnDefs = [
        {headerName: "Fecha", field: "log_created_at", sort: "desc"},
        {headerName: "Nombre del usuario", field: "fullname"},
        {headerName: "Navegador / S.O. / IP", field: "log_content"},
        {headerName: "Módulo", field: "log_class"},
        {headerName: "Detalles del evento", field: "log_special"},
        {headerName: "Observaciones", field: "log_obs"},
        // {headerName: "Datos recibidos del usuario", field: "log_post_data"},
        // {headerName: "Información de la sesión del usuario", field: "log_session_data"},
      ];
      var gridOptions = Grid.buildGripOptions(columnDefs, r, User.onSelectionChanged, true, false, false, true, -1, 'single', 'search_user');
      marssoft.in.principal.subs.user.rungrid = gridOptions;
      $('.table_place').html('');
      new agGrid.Grid($('.table_place')[0], gridOptions);
    });
  }

  run(){
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.user.run, 'Usuarios']
    ]);
    $('.content').html(`
      <div class="col s12">
        <div class="card-panel z-depth-4">
          <div class="row accessBottom">
            <div class="col s6 m2">
              ${Form.btn(false, 'waves_effect white_text primary_background', 'new_user btn100 ', 'Nuevo', 'add')}
            </div>
            <div class="col s6 m2">
              ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_user btn100 disabled', 'Editar', 'edit')}
            </div>
            <div class="col s12 m2">
              ${Form.btn(false, 'waves_effect white_text red', 'delete_user btn100 disabled', 'Borrar', 'remove')}
            </div>
            <div class="col s12 m6 input-field search_box">
              ${Form.input('search_user', 'text', 'Buscar...', '', '', '', 100, '', 'autocomplete="off"')}
            </div>
            <div class="col s12 divider"></div>
            <div class="col s12">
              <div id="myGrid" style="height: 600px; width:100%;" class="table_place ag-theme-material"></div>
            </div>
          </div>
        </div>
      </div>
    `);
    if ($.cookie('use_profile').toSimpleNumber() == 1){
      $('.search_box').addClass('m5').removeClass('m6');
      $('.accessBottom').prepend(`
        <div class="col s6 m1">
          ${Form.btn(false, 'waves_effect white_text primary_background', 'access_user center-icon btn100 disabled', '', 'visibility')}
        </div>
      `);
    }
    marssoft.asyn.post({ class: 'users', method: 'get'}, (r) => {
      let columnDefs = [
        {headerName: "usuario", field: "use_id"},
        {headerName: "Nombre de usuario", field: "fullname", sort: 'asc'},
        {headerName: "Privilegios", field: "profile"},
        {headerName: "Teléfono", field: "phone"},
      ];
      let rowData = [];
      $.each(r, (i, e) => {
        rowData.push({
          use_id : e.use_id,
          fullname: e.personal.fulllast,
          profile : e.profile,
          email: e.personal.per_email,
          phone: e.personal.per_phone,
        });
      });
      var gridOptions = Grid.buildGripOptions(columnDefs, rowData, User.onSelectionChanged, true, false, false, true, -1, 'single', 'search_user');
      marssoft.in.principal.subs.user.rungrid = gridOptions;
      new agGrid.Grid($('.table_place')[0], gridOptions);
    });
  }

  static onSelectionChanged(){
    var selectedRows = marssoft.in.principal.subs.user.rungrid.api.getSelectedRows();
    Grid.activeButtonsGrid(selectedRows[0], 'use_id', '.delete_user, .edit_user, .access_user, .detail_usage');
    if ($('.detail_usage').length > 0){
      $('.detail_usage').click();
    }
  }
}

class Login {
  constructor() {
    this.clickEvents();
  }
  clickEvents() {
    u.click('.login_action', this.login_action);
    u.click('.forgot_password', this.forgot_password);
    u.click('.sendCodeConfirmation', this.sendCodeConfirmation);
    u.click('.confirmCodeConfirmation', this.confirmCodeConfirmation);
    u.click('.changePassword', this.changePassword);
  }

  changePassword(){
    let vars = ['per_password', 'per_confirm_password'];
    let data = z.gValues(vars);
    z.mandatory(data, vars, true);
    if (!z.mandatory(data, vars)){
      Core.toast("Complete la información de los correos para continuar", "red");
      return false;
    }
    if (data.per_password != data.per_confirm_password){
      Core.toast("Las nuevas contraseñas no coinciden", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'login', method: 'changePassword', data: data}, (r) => {
      switch(r.response){
        case 'true':
          $('.cntxdnd').click();
          Core.toast("Contraseña cambiada con éxito", "green");
        break;
        case 'error':
          Core.toast("Se presentó un problema para cambiar su contraseña. Intente nuevamente", "red");
        break;
      }
    });
  }

  confirmCodeConfirmation(){
    let vars = ['per_code_use'];
    let data = z.gValues(vars);
    z.mandatory(data, vars, true);
    if (!z.mandatory(data, vars)){
      Core.toast("Complete la información de los correos para continuar", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'login', method: 'validateCode', data: data}, (r) => {
      switch(r.response){
        case 'true':
          $('.temp_solve, .tmp_basic').addClass('hidden');
          $('.temp_2_solve').removeClass('hidden');
        break;
        case 'error':
          Core.toast("El código ingresado no es correcto", "red");
        break;
      }
    });
  }

  sendCodeConfirmation(){
    let vars = ['per_email', 'per_confirm_email'];
    let data = z.gValues(vars);
    z.mandatory(data, vars, true);
    if (!z.mandatory(data, vars)){
      Core.toast("Complete la información de los correos para continuar", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'login', method: 'setMailInfo', data: data}, (r) => {
      switch(r.response){
        case 'true':
          let time = 60;
          let a = setInterval(() => {
            $('.sendCodeConfirmation').addClass('disabled').html('Podrá enviar otro correo en '+time+' segundos');
            time--;
            if (time == 0){
              $('.sendCodeConfirmation').removeClass('disabled').html('Enviar correo electrónico');
              clearInterval(a);
            }
          },1000);
          $('.temp_solve').removeClass('hidden');
        break;
        case 'mailError':
          Core.toast("El correo electrónico no coincide con el registrado en el servidor", "red");
        break;
      }
    });
    console.log(data);
  }

  forgot_password(){
    let vars = ['username'];
    let data = z.gValues(vars);
    z.mandatory(data, vars, true);
    if (!z.mandatory(data, vars)){
      Core.toast("Escriba su nombre de usuario para iniciar el proceso de recuperación", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'login', method: 'forgotPassword', data: data }, (r) => {
      switch(r.response){
        case 'cannot_restore':
          Core.toast("Por motivos de seguridad, no es posible iniciar el proceso de recuperación de esta cuenta.", "red");
        break;
        case 'locked':
          Core.toast("Por motivos de seguridad, no podrá iniciar el proceso de recuperación de contraseña hasta después de "+r.time_at, "red");
        break;
        case 'hasnt_mail':
          Core.toast("No se puede iniciar la recuperación de contraseña dado que no hay correos electrónicos asignados.", "red");
        break;
        case 'true':
          let schools = Util.buildselect(r.data, 'sch_id', 'sch_mail');

          marssoft.ui.question('Recuperación de contraseña',`
            <div class="row">
              <div class="col s12 tmp_basic input-field">
                ${Form.select('per_email', 'Seleccione un correo de recuperación ', schools)}
              </div>
              <div class="col s12 tmp_basic input-field select_use_gra_id">
                ${Form.input('per_confirm_email', 'text', 'Complete el correo electrónico seleccionado', '', '', '', 100, '', '')}
              </div>
              <div class="col s12 tmp_basic col_select">
                ${Form.btn(false, 'waves_effect white_text green', 'sendCodeConfirmation btn100', 'Enviar correo electrónico', 'add')}
              </div>
              <div class="col s12 input-field temp_solve hidden">
                <h6>Se ha enviado un código temporal para reestablecer su contraseña. Si el correo electrónico no ha llegado, por favor revise en bandeja de no deseados.</h6>
              </div>
              <div class="col s12 input-field temp_solve hidden">
                ${Form.input('per_code_use', 'number', 'Ingrese el código temporal', '', '', '', 100, '', '')}
              </div>
              <div class="col s12 col_select temp_solve hidden">
                ${Form.btn(false, 'waves_effect white_text green', 'confirmCodeConfirmation btn100', 'validar código de confirmación', 'add')}
              </div>
              <div class="col s12 input-field temp_2_solve hidden">
                <h6>Escriba su nueva contraseña.</h6>
              </div>
              <div class="col s12 input-field temp_2_solve hidden">
                ${Form.input('per_password', 'password', 'Ingrese su nueva contraseña', '', '', '', 100, '', '')}
              </div>
              <div class="col s12 input-field temp_2_solve hidden">
                ${Form.input('per_confirm_password', 'password', 'Confirme su nueva contraseña', '', '', '', 100, '', '')}
              </div>
              <div class="col s12 col_select temp_2_solve hidden">
                ${Form.btn(false, 'waves_effect white_text green', 'changePassword btn100', 'Cambiar contraseña', 'add')}
              </div>
            </div>
          `,'Cerrar', () => {}, () => {
            $('.per_email').formSelect();
          });
        break;
      }
    });
  }

  login_action(v){
    let vars = ['username', 'password'];
    let data = z.gValues(vars);
    z.mandatory(data, vars, true);
    if (!z.mandatory(data, vars)){
      Core.toast("Escriba su nombre de usuario y contraseña para continuar", "red");
      return false;
    }
    v.addClass('disabled').html('Iniciando sesión...');
    marssoft.asyn.post({ class: 'login', method: 'grant', data: data }, (r) => {
      v.removeClass('disabled').html('Iniciar sesión');
      switch(r.response){
        case 'denied':
          Core.toast("Acceso denegado, intente nuevamente", "red");
          break;
        case 'locked':
          Core.toast("Cuenta bloqueada por exceso de fallos, podrá iniciar sesión "+moment(r.time_at).fromNow(), "red");
          break;
        case 'true':
          marssoft.user = r.user_data;
          Core.toast("Hola "+marssoft.user.personal.fullname+", bienvenid"+marssoft.user.personal.adjetives.adj_3+".", "green");
          marssoft.out.login.exit(() => {
            marssoft.in.principal.run(() => {
              $('span.privilegies').html(marssoft.user.use_id+' - '+marssoft.user.use_profile+' - Último acceso: '+marssoft.user.use_last_login);
            });
          });
          break;
      }
    });
    
  }
  run() {
    $('title').html(marssoft.data.title + ' - Inicio de sesión');
    $('.app').html(`
      <div class="login scale-transition scale-out">
        <div class="card z-depth-5">
          <div class="system_img center-align"><img src="assets/img/logo_b.png" height="90"></div>
          <div class="system_title primary_color">${marssoft.data.title}</div>
          <div class="card-content row">
            <div class="input-field col s12">
              <input class="username" id="username" type="text" class="validate">
              <label for="username">Nombre de usuario</label>
            </div>
            <div class="input-field col s12">
              <input class="password" id="password" type="password" class="validate">
              <label for="password">Contraseña</label>
            </div>
          </div>
          <div class="card-action center-align">
            <a href="#!" class="login_action btn100 waves-effect waves-light btn-flat primary_background white-text right-text">Iniciar sesión</a>
            <br>
            <br>
            <a href="#" class="forgot_password primary_color">Recuperar contraseña</a>
          </div>
        </div>
      </div>
      <div class="copyright version">Última actualización: ${(moment.unix(version)).format('dddd DD [de] MMMM [de] YYYY [a las] h:mm:ss a')}</div>
    `);
    M.updateTextFields();
    setTimeout(() => {
      $('.login').addClass('scale-in');
    }, 150);
  }
  exit(fn){
    $('.login').removeClass('scale-in');
    setTimeout(fn, 150);
  }
}