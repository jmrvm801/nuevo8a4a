const makeCell = (content, rowIndex = -1, options = {}) => {
  return Object.assign({text: content, fillColor: rowIndex % 2 ? 'white' : '#e8e8e8'}, options);
}
const thl = (content, rowIndex = -1, options = {}) => {
    return makeCell(content, rowIndex, Object.assign({ bold: true, alignment: 'left', fontSize: 9 }, options));
}
const thr = (content, rowIndex = -1, options = {}) => {
    return makeCell(content, rowIndex, Object.assign({ bold: true, alignment: 'right', fontSize: 9 }, options));
}
const tdl = (content, rowIndex = -1, options = {}) => {
    return makeCell( content, rowIndex, Object.assign({ bold: false, alignment: 'left', fontSize: 9 }, options));
}
const tdr = (content, rowIndex = -1, options = {}) => {
    return makeCell( content, rowIndex,Object.assign({ bold: false, alignment: 'right', fontSize: 9 }, options));
}
const createDocumentDefinition = (reportDate, subHeading, ...contentParts) => {
  const baseDocDefinition = {
      pageSize: 'A4',
      footer: (currentPage, pageCount) => {
          return {
              text: `Page ${currentPage.toString()} of ${pageCount.toString()}`,
              alignment: 'center',
              fontSize: 7
          }
      },

      styles: {
          title: {
              fontSize: 24
          },
          titleSub: {
              fontSize: 18
          },
          titleDate: {
              fontSize: 14,
              alignment: 'right',
              bold: true
          }
      },

      content: [
          {
              columns: [
                  {text: '', style: 'title', width: '*'},
                  {text: reportDate, style: 'titleDate', width: '160'},
              ]
          },
          {text: `${subHeading}\n\n`, style: 'titleSub'},
      ],
  };
  const docDefinition = JSON.parse(JSON.stringify(baseDocDefinition));
  docDefinition.footer = baseDocDefinition.footer;
  docDefinition.content.push(...contentParts);
  return docDefinition;
};