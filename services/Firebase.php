<?php

include_once('Version.php');

/**
 * THIS IS AN EXAMPLES THAT MUST BE FOLLOWED IN ORDER TO MAKE A WELL-FORMED PUSH NOTIFICATIONS
 * THROUGH FIREBASE CLOUD MESSAGING IN APPLE AND ANDROID DEVICES.
 * 
 * @author Fernando Carreón
 */

/*
=============== BUILD A SIMPLE NOTIFICATION ===============
{
    "class":"Firebase",
    "method":"sendSimpleNotification",
    "token": "756489102d2b4d16",
    "data": {
        "devices" : [
            "dholcyKRzm0:APA91bEp2J7S8V7LnLV2mzlwLq4mkXnxh_e78PGCJi3-L35q53OGitSEInYfITX5tF-pJh2QxWL9shmyfAr_oUs3Q8VIDGhNaWnM6hYI-HqJzRNjpjDerDlst3p3Jf3M0cM-44yMTmSg"
        ],
        "way" : "android",
        "title" : "Hamburguesas 15% OFF",
        "body" : "Disfruta de una anvorguesa con doble queso",
        "channel" : "notificacion11010387",
        "sound" : "notificacion",
        "data" : {
            "aditionaldata" : "to make custom callback actions"
        }
    }
}
=============== BUILD A IMAGE NOTIFICATION ===============
{
    "class":"Firebase",
    "method":"sendImageNotification",
    "token": "756489102d2b4d16",
    "data": {
        "devices" : [
            "dholcyKRzm0:APA91bEp2J7S8V7LnLV2mzlwLq4mkXnxh_e78PGCJi3-L35q53OGitSEInYfITX5tF-pJh2QxWL9shmyfAr_oUs3Q8VIDGhNaWnM6hYI-HqJzRNjpjDerDlst3p3Jf3M0cM-44yMTmSg"
        ],
        "way" : "android",
        "title" : "Lo nuevo de Beatrich",
        "body" : "Escucha Hands Tied por tu App favorita",
        "channel" : "notificacion11010387",
        "sound" : "notificacion",
        "image" : "https://giggoer.com/wp-content/uploads/2020/03/Beatrich-2020-595x890.jpg",
        "data" : {
            "aditionaldata" : "to make custom callback actions"
        }
    }
}
=============== BUILD A ONGOING NOTIFICATION ===============
{
    "class":"Firebase",
    "method":"sendOngoingNotification",
    "token": "756489102d2b4d16",
    "data": {
        "devices" : [
            "dholcyKRzm0:APA91bEp2J7S8V7LnLV2mzlwLq4mkXnxh_e78PGCJi3-L35q53OGitSEInYfITX5tF-pJh2QxWL9shmyfAr_oUs3Q8VIDGhNaWnM6hYI-HqJzRNjpjDerDlst3p3Jf3M0cM-44yMTmSg"
        ],
        "way" : "android",
        "title" : "Llamada entrante...",
        "body" : "Pedrito Sola",
        "channel" : "notificacion11010387",
        "sound" : "notificacion",
        "data" : {
            "aditionaldata" : "to make custom callback actions"
        }
    }
}
=============== BUILD A ONGOING-IMAGE NOTIFICATION ===============
{
    "class":"Firebase",
    "method":"sendOngoingImageNotification",
    "token": "756489102d2b4d16",
    "data": {
        "devices" : [
            "dholcyKRzm0:APA91bEp2J7S8V7LnLV2mzlwLq4mkXnxh_e78PGCJi3-L35q53OGitSEInYfITX5tF-pJh2QxWL9shmyfAr_oUs3Q8VIDGhNaWnM6hYI-HqJzRNjpjDerDlst3p3Jf3M0cM-44yMTmSg"
        ],
        "way" : "android",
        "title" : "Llamada entrante...",
        "body" : "Pedrito Sola",
        "channel" : "notificacion11010387",
        "sound" : "notificacion",
        "image" : "https://heraldodemexico.com.mx/u/fotografias/m/2020/7/14/f608x342-142_29865_0.jpg",
        "data" : {
            "aditionaldata" : "to make custom callback actions"
        }
    }
}
=============== BUILD A CALLBACK NOTIFICATION ===============
{
    "class":"Firebase",
    "method":"sendCallbackNotification",
    "token": "756489102d2b4d16",
    "data": {
        "devices" : [
            "dholcyKRzm0:APA91bEp2J7S8V7LnLV2mzlwLq4mkXnxh_e78PGCJi3-L35q53OGitSEInYfITX5tF-pJh2QxWL9shmyfAr_oUs3Q8VIDGhNaWnM6hYI-HqJzRNjpjDerDlst3p3Jf3M0cM-44yMTmSg"
        ],
        "way" : "android",
        "title" : "Llamada entrante...",
        "body" : "Pedrito Sola",
        "channel" : "notificacion11010387",
        "sound" : "notificacion",
        "data" : {
            "aditionaldata" : "to make custom callback actions"
        },
        "actions" : [
            {
                "icon" : "myicon",
                "title" : "Responderle a Pedrito bebé",
                "callback" : "answerCall"
            },
            {
                "icon" : "myicon",
                "title" : "Rechazar",
                "callback" : "rejectCall"
            }
        ]
    }
}

IMPORTANT
The callback functions like answerCall & rejectCall must be implemented on the index.js before the request is sent

push.on('answerCall', (data) => {
    //Do something
});
push.on('rejectCall', (data) => {
    //Do something
});

=============== BUILD A CALLBACK-ONGOING NOTIFICATION ===============
{
    "class":"Firebase",
    "method":"sendCallbackOngoingNotification",
    "token": "756489102d2b4d16",
    "data": {
        "devices" : [
            "dholcyKRzm0:APA91bEp2J7S8V7LnLV2mzlwLq4mkXnxh_e78PGCJi3-L35q53OGitSEInYfITX5tF-pJh2QxWL9shmyfAr_oUs3Q8VIDGhNaWnM6hYI-HqJzRNjpjDerDlst3p3Jf3M0cM-44yMTmSg"
        ],
        "way" : "android",
        "title" : "Llamada entrante...",
        "body" : "Pedrito Sola",
        "channel" : "notificacion11010387",
        "sound" : "notificacion",
        "data" : {
            "aditionaldata" : "to make custom callback actions"
        },
        "actions" : [
            {
                "icon" : "myicon",
                "title" : "Responderle a Pedrito bebé",
                "callback" : "answerCall"
            },
            {
                "icon" : "myicon",
                "title" : "Rechazar",
                "callback" : "rejectCall"
            }
        ]
    }
}

IMPORTANT
The callback functions like answerCall & rejectCall must be implemented on the index.js before the request is sent

push.on('answerCall', (data) => {
    //Do something
});
push.on('rejectCall', (data) => {
    //Do something
});

=============== BUILD A CALLBACK-IMAGE NOTIFICATION ===============
{
    "class":"Firebase",
    "method":"sendCallbackImageNotification",
    "token": "756489102d2b4d16",
    "data": {
        "devices" : [
            "dholcyKRzm0:APA91bEp2J7S8V7LnLV2mzlwLq4mkXnxh_e78PGCJi3-L35q53OGitSEInYfITX5tF-pJh2QxWL9shmyfAr_oUs3Q8VIDGhNaWnM6hYI-HqJzRNjpjDerDlst3p3Jf3M0cM-44yMTmSg"
        ],
        "way" : "android",
        "title" : "Llamada entrante...",
        "body" : "Pedrito Sola",
        "channel" : "notificacion11010387",
        "sound" : "notificacion",
        "image" : "https://heraldodemexico.com.mx/u/fotografias/m/2020/7/14/f608x342-142_29865_0.jpg",
        "data" : {
            "aditionaldata" : "to make custom callback actions"
        },
        "actions" : [
            {
                "icon" : "myicon",
                "title" : "Responderle a Pedrito bebé",
                "callback" : "answerCall"
            },
            {
                "icon" : "myicon",
                "title" : "Rechazar",
                "callback" : "rejectCall"
            }
        ]
    }
}

IMPORTANT
The callback functions like answerCall & rejectCall must be implemented on the index.js before the request is sent

push.on('answerCall', (data) => {
    //Do something
});
push.on('rejectCall', (data) => {
    //Do something
});

=============== BUILD A CALLBACK-IMAGE-ONGOING NOTIFICATION ===============
{
    "class":"Firebase",
    "method":"sendCallbackOngoingImageNotification",
    "token": "756489102d2b4d16",
    "data": {
        "devices" : [
            "dholcyKRzm0:APA91bEp2J7S8V7LnLV2mzlwLq4mkXnxh_e78PGCJi3-L35q53OGitSEInYfITX5tF-pJh2QxWL9shmyfAr_oUs3Q8VIDGhNaWnM6hYI-HqJzRNjpjDerDlst3p3Jf3M0cM-44yMTmSg"
        ],
        "way" : "android",
        "title" : "Llamada entrante...",
        "body" : "Pedrito Sola",
        "channel" : "notificacion11010387",
        "sound" : "notificacion",
        "image" : "https://heraldodemexico.com.mx/u/fotografias/m/2020/7/14/f608x342-142_29865_0.jpg",
        "data" : {
            "aditionaldata" : "to make custom callback actions"
        },
        "actions" : [
            {
                "icon" : "myicon",
                "title" : "Responderle a Pedrito bebé",
                "callback" : "answerCall",
                "replyLabel" : "Responder con un mensaje"
            },
            {
                "icon" : "myicon",
                "title" : "Rechazar",
                "callback" : "rejectCall"
            }
        ]
    }
}
IMPORTANT
The callback functions like answerCall & rejectCall must be implemented on the index.js before the request is sent

push.on('answerCall', (data) => {
    //Do something
});
push.on('rejectCall', (data) => {
    //Do something
});

IF A REPLY IS NEEDED, YOU CAN ALSO CHANGE THE ACTION PARAMETERS WITH THE FOLLOWING CODE

"actions" : [
            {
                "icon" : "myicon",
                "title" : "Responderle a Pedrito bebé",
                "callback" : "answerCall",
                "foreground" : true,
                "inline" : true,
                "replyLabel" : "Responder con un mensaje"
            },
            {
                "icon" : "myicon",
                "title" : "Rechazar",
                "callback" : "rejectCall",
                "foreground" : true,
                "inline" : true,
                "replyLabel" : "Rechazar con un mensaje"
            }
        ]
*/

class Firebase {

  public $priority = 'high';
  public $color = '#9933ff';
  public $icon = 'myicon';

  public function __construct($priority = 'high', $color = '#9933ff', $icon = 'myicon'){
    $this->priority = $priority;
    $this->color = $color;
    $this->icon = $icon;
  }

  public function run($method){
    switch($method){
      case 'sendSimpleNotification':
        return $this->sendNotification(array('basic'), $_POST['data']);
      case 'sendImageNotification':
        return $this->sendNotification(array('image'),$_POST['data']);
      case 'sendOngoingNotification':
        return $this->sendNotification(array('ongoing'),$_POST['data']);
      case 'sendOngoingImageNotification':
        return $this->sendNotification(array('ongoing', 'image'),$_POST['data']);
      case 'sendCallbackNotification':
        return $this->sendNotification(array('callback'),$_POST['data']);
      case 'sendCallbackOngoingNotification':
        return $this->sendNotification(array('callback', 'ongoing'),$_POST['data']);
      case 'sendCallbackImageNotification':
        return $this->sendNotification(array('callback', 'image'),$_POST['data']);
      case 'sendCallbackOngoingImageNotification':
        return $this->sendNotification(array('callback', 'image', 'ongoing'),$_POST['data']);
    }
  }

  /* 
    Send a basic notification through FCM   
  */

  public function sendNotification($action, $data){
    if ($data['way'] == 'android'){
      $msg = $this->createAndroidMessage($action, $data);
      $fields = $this->buildAndroidFields($data['devices'], $msg);
    } else {
      $msg = $this->createIosMessage($action, $data);
      $fields = $this->buildAppleFields($data['devices'], $msg, $data['data']);
    }
    return json_decode($this->postFCM($fields));
  }


  /**
   * Build a well-formed android query
   * $devices array: A FCM Tokens valid for android devices
   * $msg Array:
   */

  public function buildAndroidFields($devices, $msg){
    return array(
      'registration_ids' => $devices, 
      'data' => $msg, 
      "content_available"=> true,
      'priority' => 'high'
    );
  }

  /**
   * Build a well-formed Apple query
   */

  public function buildAppleFields($devices, $msg, $data){
    return array(
      'registration_ids'  => $devices, 
      'notification' => $msg,
      'data' => $data, 
      "content_available"=> true,
      'priority' => 'high',
    );
  }

  /* Build a well-formed android array params */

  public function createAndroidMessage($type, $data){
    $msg = array(
      'title'=> $data['title'], 
      'body'=> $data['body'], 
      'android_channel_id'=> $data['channel'],
      'color' => $this->color,
      'sound' => $data['sound'],
      'soundname' => $data['sound'],
      'badge' => '0',
      'icon' => 'myicon',
      'mutable-content' => true
    );
    for($i = 0; $i < count($type); $i++){
      switch($type[$i]){
        case 'image':
          $msg = $this->addImageToMessage($msg, $data);
        break;
        case 'ongoing':
          $msg = $this->addOngoingToMessage($msg, $data);
        break;
        case 'callback':
          $msg = $this->addCallbackToMessage($msg, $data);
        break;
      }
    }

    $msg = array_merge($msg, $data['data']);
    return $msg;
  }

  public function addCallbackToMessage($msg, $data){
    foreach($data['actions'] as $i => $value){
      if (isset($data['actions'][$i]['foreground']))
        $data['actions'][$i]['foreground'] = ($data['actions'][$i]['foreground'] == 1) ? true : false;
      if (isset($data['actions'][$i]['inline']))
        $data['actions'][$i]['inline'] = ($data['actions'][$i]['inline'] == 1) ? true : false;
    }
    $msg['actions'] = $data['actions'];
    return $msg;
  }

  /* Add a picture to push notification */

  public function addOngoingToMessage($msg, $data){
    $msg['ongoing'] = true;
    return $msg;
  }

  /* Add a picture to push notification */

  public function addImageToMessage($msg, $data){
    $msg['style'] = 'picture';
    $msg['summaryText'] = $data['body'];
    $msg['picture'] = $data['image'];
    return $msg;
  }

  /* Build a well-formed Apple array params */

  public function createIosMessage($type, $data){
    $msg = array(
      'title'=> $data['title'], 
      'body'=> $data['body'], 
      'sound' => $data['sound'], //caf extension is mandatory for soundname
      'android_channel_id'=> $data['channel'],
      'color' => '#3f51b5', 
    );
    return $msg;
  }

  /* Send the request to FCM Cloud Services */

  public function postFCM($fields){
    $headers = array('Authorization: key=' . Version::$apiToken,'Content-Type: application/json');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt($ch, CURLOPT_POST, true );
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec($ch );
    curl_close( $ch );
    return $result;
  }
}
?>