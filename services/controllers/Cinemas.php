<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Cinemas extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }

  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['data']);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
      case 'getRoomsByCinemaId':
        return $this->getRoomsByCinemaId($_POST['id']);
    }
  }

  public function getRoomsByCinemaId($id){
    $d = new db();
    $d->q("SELECT roo_id FROM rooms WHERE roo_cin_id = '$id' AND roo_deleted = '0';"); //La clase de MySQL que gestiona todas las consultas
    $array = array();
    $rooms = new Rooms();
    while($row = $d->fa()){ //Recorrerá todos los registros que haya traido MySQL
      $room = $rooms->single($row['roo_id']);
      array_push($array, $room);
    }
    $d->cl();
    return $array;
  }

  public function getMoviesByCinema($id){
    $d = new db();
    $d->q("SELECT roo_id FROM cinemas INNER JOIN rooms ON rooms.roo_cin_id=cinemas.cin_id INNER JOIN rooms_functions ON rooms.roo_id=rooms_functions.roo_id INNER JOIN functions ON rooms_functions.fun_id=functions.fun_id;"); //La clase de MySQL que gestiona todas las consultas
    $array = array();
    $rooms = new Rooms();
    while($row = $d->fa()){ //Recorrerá todos los registros que haya traido MySQL
      $room = $rooms->single($row['roo_id']);
      array_push($array, $room);
    }
    $d->cl();
    return $array;
  }
  /**para que funcione ejecutar este query
  CREATE TABLE `rooms_functions` ( 
    `roo_fun_id` INT(10) NOT NULL PRIMARY KEY , 
    `roo_id` INT(10) NOT NULL , 
    `fun_id` INT(10) NOT NULL 
);
en la BD 
*/


  /** Eliminación de un Cinema */

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  /** Acceso a un Cinema */

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $_SESSION['cin_id'] = $single['cin_id'];
    $single = $this->xss_client($single);
    return $single;
  }

  /** Acceso a la lista de Cinemas */

  public function get(){
    $this->sget(Ws::$c);
    $array = array();
    while($row = Ws::$c->fa()){
      $client = $this->xss_client($row);
      array_push($array, $client);
    }
    return $array;
  }

  /** Actualización de un Cinema*/

  public function update($data){
    try {
      //$data['cli_birthday'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['cli_birthday'])));
      $this->upd($_SESSION['cli_id'], $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  /** Creación de un Cinema */

  public function create($data){
    try {
      //$data['cli_birthday'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['cli_birthday'])));
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table(0);
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }

  public $foreign_keys = array(
      
  );
  
  public $rows = array(
    array('branch_name', 'varchar(100)', 'NOT NULL'),
    array('location', 'varchar(255)', 'NOT NULL'),
    array('phone', 'varchar(10)', 'NOT NULL'),
   
   

    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>