<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Categories extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }

  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        $id = isset($_SESSION["cat_id"]) ? $_SESSION["cat_id"] : $_POST['id'];
        return $this->update($_POST['data'], $id);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);

    }
  }

  /** Eliminación de una Categories */

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  /** Acceso a una Categories única */

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $_SESSION['cat_id'] = $id;
    $single = Ws::$c->fa();
    $single = $this->xss_client($single);
    return $single;
  }

  /** Acceso a la lista de Categories */

  public function get(){
    $this->sget(Ws::$c, "", "cat_name");
    $array = array();
    while($row = Ws::$c->fa()){
      $row = $this->xss_client($row);
      array_push($array, $row);
    }
    return $array;
  }

  /** Actualización de una Categories */

  public function update($data, $id){
    $data = $this->utf8_server($data);
    try {
      $this->upd($id, $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  /** Creación de una Categories*/

  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table();
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }
  /**
   * Set foreign keys 
   */
  public $foreign_keys = array(
    array('cli_id', 'int(6)', 'NOT NULL', 'clients'),
  );
  /**
   * Set row keys
   */
  public $rows = array(
    array('name', 'varchar(50)', 'NOT NULL'),
    array('costo', 'int(10)', 'NOT NULL'),

    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>