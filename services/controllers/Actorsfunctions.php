<?php
include_once("QueryBuilder.php"); //package com.java.proyecto;
include_once("SqlManagement.php");
class Actorsfunctions extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this); //super ()
  }

  public function run($method = 'default'){ // public static void main (string ... args)
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        $id = isset($_SESSION["act_id"]) ? $_SESSION["act_id"] : $_POST['id'];
        return $this->update($_POST['data'], $id);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }

  /** Eliminación de un Actorsfunctions */

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  /** Acceso a un Actorsfunctions única */

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $_SESSION['act_id'] = $id;
    $single = Ws::$c->fa();
    $single = $this->xss_client($single);
    return $single;
  }

  /** Acceso a la lista de Actorsfunctions */

  public function get(){
    $this->sget(Ws::$c, "", "act_name");
    $array = array();
    while($row = Ws::$c->fa()){
      $row = $this->xss_client($row);
      array_push($array, $row);
    }
    return $array;
  }

  /** Actualización de un Actorsfunctions */

  public function update($data, $id){
    $data = $this->utf8_server($data);
    try {
      $this->upd($id, $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  /** Creación de un Actorsfunctions */

  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table();
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array(
    array('fun_id', 'int(7)', 'NOT NULL', 'functions'),
    array('act_id', 'int(7)', 'NOT NULL', 'actors'),

  );
  /**
   * Set row keys
   */
  public $rows = array(
    array('deleted', 'varchar(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>