<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Transactions extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }

  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['data']);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }

  /** Eliminación de una Transaction */

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  /** Acceso a una Transaction */

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $_SESSION['cli_id'] = $single['cli_id'];
    $single = $this->xss_client($single);
    $single['cli_birthday'] = $this->toDate($single['cli_birthday']);
    return $single;
  }

  /** Acceso a la lista de Transaction */

  public function get(){
    $this->sget(Ws::$c);
    $array = array();
    while($row = Ws::$c->fa()){
      $client = $this->xss_client($row);
      array_push($array, $client);
    }
    return $array;
  }

  /** Actualización de una Transaction*/

  public function update($data){
    try {
      $data['cli_birthday'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['cli_birthday'])));
      $this->upd($_SESSION['cli_id'], $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  /** Creación de una Transaction */

  public function create($data){
    try {
      $data['cli_birthday'] = date('Y-m-d', strtotime(Ws::$g->stringDateToNumber($data['cli_birthday'])));
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table(0);
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }

  public $foreign_keys = array(
    array('mem_id', 'int (7)', 'NOT NULL', 'memberships')
      
  );
  
  public $rows = array(
    array('total', 'int(10)', 'NOT NULL'),
    array('way_to_pay', 'int(10)', 'NOT NULL'),
   
   
    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}