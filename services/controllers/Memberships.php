<?php
include_once("QueryBuilder.php"); //package com.java.proyecto;
include_once("SqlManagement.php");
class Memberships extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this); //super ()
  }

  public function run($method = 'default'){ // public static void main (string ... args)
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        $id = isset($_SESSION["mem_id"]) ? $_SESSION["mem_id"] : $_POST['id'];
        return $this->update($_POST['data'], $id);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }

  public function getmembersWithmembership($id){//conseguir los miembros con membresia 
    $d = new db();
    $d->q("SELECT memberships.cli_id FROM `memberships` INNER JOIN clients ON memberships.cli_id = clients.cli_id;"); //La clase de MySQL que gestiona todas las consultas
    $array = array();
    $Clients = new Clients();
    while($row = $d->fa()){ //Recorrerá todos los registros que haya traido MySQL
      $Client = $Clients->single($row['cli_id']);
      array_push($array, $Client);
    }
    $d->cl();
    return $array;
  }
  /*para que funcione este comando ejecutar query
  ALTER TABLE `memberships` ADD COLUMN cli_id VARCHAR(100) NOT NULL
  */

  /** Eliminación de un Memberships */

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  /** Acceso a un Memberships única */

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $_SESSION['mem_id'] = $id;
    $single = Ws::$c->fa();
    $single = $this->xss_client($single);
    return $single;
  }

  /** Acceso a la lista de Memberships */

  public function get(){
    $this->sget(Ws::$c, "", "mem_name");
    $array = array();
    while($row = Ws::$c->fa()){
      $row = $this->xss_client($row);
      array_push($array, $row);
    }
    return $array;
  }

  /** Actualización de un Memberships */

  public function update($data, $id){
    $data = $this->utf8_server($data);
    try {
      $this->upd($id, $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  /** Creación de un Memberships */

  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table();
    if (!$this->is_ok($this->foreign_keys, $this->rows)){
      $this->create_fields($this->foreign_keys, 0, true);
      $this->create_fields($this->rows, 0);
    }
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array(
    array('use_id', 'varchar(30)', 'NOT NULL', 'users'),
  );
  /**
   * Set row keys
   */
  public $rows = array(
    array('headline', 'varchar(20)', 'NOT NULL'),
    array('type', 'varchar(20)', 'NOT NULL'),
    array('points', 'varchar(10)', 'NOT NULL'),
    array('member_since', 'datetime', 'NOT NULL'),

    
    array('deleted', 'varchar(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>