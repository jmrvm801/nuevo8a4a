<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Expose-Headers: Content-Length');
date_default_timezone_set("America/Mexico_City"); //GTM-5
include_once('db.php');
include_once('gral.php');
include_once('Util.php');
include_once('Version.php');
include_once('Firebase.php');
$runsql = array();
foreach (glob("controllers/*.php") as $file){
	include_once($file);
	$class = explode('.', explode('/', $file)[1])[0];
	$array = array('QueryBuilder', 'SqlManagement', 'MarssoftError');
	if (array_search($class, $array) === false)
		if (count(class_implements($class)) > 0)
			array_push($runsql, $class);
}

class Ws extends Util{
	public static $g = '';
	public static $c = '';

	public function __construct($runsql, $token){
		Ws::$g = new Gral();
		Ws::$c = new db();
		if ($token != ''){
			Ws::$c->q("SELECT use_id FROM users WHERE use_token LIKE '%$token%' LIMIT 1;");
			if (Ws::$c->nr() > 0){
				$array = array(
					'username' => Ws::$c->r(),
					'password' => ''
				);
				(new Login())->grant($array, true);
			} else {
				header('HTTP/1.0 403 Forbidden');
				Ws::$g->kill((json_encode(array('response' => 'invalid_token'))));
			}
		}
		$this->genCookies();
		if ($_POST['class'] != 'alive')
			$this->run_sql($runsql);
	}

	public function run_sql($runsql){
		$config = new Config();
		Ws::$c->q("SHOW TABLES LIKE 'logs';");
		if (Ws::$c->nr() == 0){
			$this->update($runsql);
			(new Users())->base_user();
		} else if ($config->performUpdate()){
			$this->update($runsql);
		}
	}

	public function update($runsql){
		for($i = 0; $i < count($runsql); $i++){
			$rule = (new ReflectionClass($runsql[$i]))->newInstanceArgs(array());
			$rule->sql_rules();
		}
	}

	public function genCookies(){
		foreach ($_COOKIE as $clave => $valor)
			Ws::$g->cook($clave, $valor, 7200);
	}

	public function run($class, $token){
		if ($class == 'alive'){
			Ws::$g->kill(json_encode(array('response' => 'ok')));
		}
		$obs = ($token == '') ? '' : 'Acceso por servicio web';
		(new Logs())->create($class, $_POST['method'], $obs);
		$login = new Login();
		switch ($class) {
			case 'login':
				Ws::$g->kill(json_encode($login->run($_POST['method'])));
				break;
			default:
				if (!$login->logged()['response'])
					Ws::$g->kill(json_encode(array('denied')));
				$reflect = (new ReflectionClass($class))->newInstanceArgs(array());
				$data = json_encode($reflect->run($_POST['method']));
				header("X-Content-Length: ".strlen($data));
				Ws::$g->kill($data);
				break;
		}
	}

	public static function e($value = ''){
		if (is_array($value)){
			echo '<pre>';
			print_r($value);
			echo '</pre>';
		} else {
			echo date('Y-m-d H:i.s').':- '.$value.'<br>';
		}
	}

	public function __destruct(){
		if (Ws::$c != '')
			Ws::$c->cl();
	}
}

$cont = true;
$tokenize = '';
$token = json_decode(file_get_contents("php://input"), true);
if (is_array($token)){
	$_POST = $token;
	if (!isset($_POST['token'])){
		header('HTTP/1.0 403 Forbidden');
		echo (json_encode(array('response' => 'missing_token')));
		$cont = false;
	} else {
		$tokenize = $_POST['token'];
	}
}
if (isset($_POST['class']) && $cont) 
	(new Ws($runsql, $tokenize))->run($_POST['class'], $tokenize);